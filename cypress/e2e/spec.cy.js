/// <reference types='cypress'>
import homePage from "../pageObjects/homePage";
import formsPage from "../pageObjects/formsPage";
import practiceFormPage from "../pageObjects/praticeFormPage";
import users from '../fixtures/jsonFiles/users.json'

const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
]

function assertValid(resultDate) {
  cy.get('#example-modal-sizes-title-lg').should('contain', 'Thanks for submitting the form')
  cy.get('tbody').should('contain.text', users.validUser.firstName)
  cy.get('tbody').should('contain.text', users.validUser.lastName)
  cy.get('tbody').should('contain.text', users.validUser.userEmail)
  cy.get('tbody').should('contain.text', users.validUser.gender)
  cy.get('tbody').should('contain.text', users.validUser.userNumber)
  cy.get('tbody').should('contain.text', resultDate)
}

describe('fill and validate form', () => {
  beforeEach('navigate to practice form page', () => {
    cy.visit('/')
    cy.on('uncaught:exception', (err, runnable) => {
      return false
    })
    homePage.naivgateToForms();
    formsPage.navigateToPracticeFormPage();
  })

  it('should validate with all valid user data', () => {
    practiceFormPage.typeFirstName(users.validUser.firstName)
    practiceFormPage.typeLastName(users.validUser.lastName)
    practiceFormPage.typeUserEmail(users.validUser.userEmail)
    practiceFormPage.chooseGender(users.validUser.gender)
    practiceFormPage.typeUserNumber(users.validUser.userNumber)
    practiceFormPage.chooseDateOfBirth(users.validUser.birthYear, users.validUser.birthMonth, users.validUser.birthDayLabel)
    practiceFormPage.typeSubject(users.validUser.subject)
    practiceFormPage.chooseHobbies(users.validUser.hobbies)
    practiceFormPage.selectImage(users.validUser.imagePath)
    practiceFormPage.typeAddress(users.validUser.address)
    practiceFormPage.chooseState(users.validUser.state)
    practiceFormPage.chooseCity(users.validUser.city)
    practiceFormPage.submit()

    assertValid(users.validUser.resultBirthDate)
  })

  it('should validate with only firstName, LastName, email, gender, number', function () {
    practiceFormPage.typeUserNumber(users.validUser.userNumber)
    practiceFormPage.typeFirstName(users.validUser.firstName)
    practiceFormPage.typeLastName(users.validUser.lastName)
    practiceFormPage.typeUserEmail(users.validUser.userEmail)
    practiceFormPage.chooseGender(users.validUser.gender)
    practiceFormPage.submit()

    const today = new Date()
    assertValid(`${today.getDate().toString().padStart(2, "0")} ${months[today.getMonth()]},${today.getFullYear()}`);

  });

  it('should not validate with invalid email', () => {
    practiceFormPage.typeUserEmail(users.invalidUser.userEmail)
    practiceFormPage.submit()

    cy.get('#example-modal-sizes-title-lg').should('not.exist')
  });

  it('should not validate with invalid number', function () {
    practiceFormPage.typeUserNumber(users.invalidUser.userNumber)
    practiceFormPage.submit()

    cy.get('#example-modal-sizes-title-lg').should('not.exist')
  });
})
