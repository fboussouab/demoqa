class practiceFormPage {
    elements = {
        addressInput: () => cy.get('#currentAddress'),
        firstNameInput: () => cy.get('#firstName'),
        lastNameInput: () => cy.get('#lastName'),
        userEmailInput: () => cy.get('#userEmail'),
        genderRadio: () => cy.get('[type="radio"]'),
        userNumberInput: () => cy.get("#userNumber"),
        dateOfBirthInput: () => cy.get('#dateOfBirthInput'),
        yearDatePicker: () => cy.get('.react-datepicker__year-select'),
        monthDatePicker : () => cy.get('.react-datepicker__month-select'),
        dayDatePicker: (dayLabel) => cy.get(`[aria-label= "${dayLabel}"]`),
        subjectInput: () => cy.get('#subjectsInput'),
        hobbiesCheckBox: (hobby) => cy.get(`#hobbies-checkbox-${hobby}`),
        imageInput: () => cy.get('#uploadPicture'),
        stateSelect: () => cy.get('#state'),
        citySelect: () => cy.get('#city'),
        submitBtn: () => cy.get('#submit'),
        userForm: () => cy.get('#userForm')
    }

    selectImage(imagPath) {
        this.elements.imageInput().selectFile(imagPath)
    }

    chooseHobbies(hobbiesList) {
        hobbiesList.forEach(hobby => {
            this.elements.hobbiesCheckBox(hobby).check({force:true});
        })
    }
    typeFirstName(firstName){
        this.elements.firstNameInput().type(firstName)
    }

    typeSubject(subject){
        this.elements.subjectInput().type(subject)
    }

    typeLastName(lastName){
        this.elements.lastNameInput().type(lastName)
    }

    typeUserEmail(userEmail){
        this.elements.userEmailInput().type(userEmail)
    }

    chooseGender(gender) {
        this.elements.genderRadio().check(gender, {force:true})
    }

    typeUserNumber(userNumber){
        this.elements.userNumberInput().type(userNumber)
    }

    chooseDateOfBirth(year, month, dayLabel) {
        this.elements.dateOfBirthInput().click()
        this.elements.yearDatePicker().select(year)
        this.elements.monthDatePicker().select(month)
        this.elements.dayDatePicker(dayLabel).click()

    }

    typeAddress(address) {
        this.elements.addressInput().type(address)
    }

    chooseState(state) {
        this.elements.stateSelect().click().contains(state).click({force:true})
    }

    chooseCity(city) {
        this.elements.citySelect().click().contains(city).click({force:true})
    }

    submit() {
        this.elements.userForm().submit()
    }
}


module.exports = new practiceFormPage();
