class formsPage {
    elements = {
        practiceFormMenuItem: () => cy.get('span').contains('Practice Form')
    }

    navigateToPracticeFormPage() {
        this.elements.practiceFormMenuItem().click()
    }
}


module.exports = new formsPage();
