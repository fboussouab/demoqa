class homePage {
    elements = {
        formsCard: () => cy.get('h5').contains('Forms')
    }

    naivgateToForms(){
        this.elements.formsCard().click();
    }
}

module.exports = new homePage();
